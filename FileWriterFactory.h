#ifndef __eloquent__FileWriterFactory__
#define __eloquent__FileWriterFactory__

// C
#include <syslog.h>

// C++
#include <vector>
#include <string>

// Boost
#include <boost/property_tree/ptree.hpp>
#include <boost/smart_ptr.hpp>

// Internal
#include "FileWriter.h"
#include "Eloquent/Extensions/IO/IOFactory.h"

namespace Eloquent {
	///////////////////////////////////////////////////////////////////////////////
	// FileWriterFactory : IOExtensionFactory
	///////////////////////////////////////////////////////////////////////////////
	class FileWriterFactory : public IOFactory {
	public:
		FileWriterFactory();
		virtual ~FileWriterFactory();
		
		virtual IO* New( const boost::property_tree::ptree::value_type& i_Config
								 , std::mutex& i_QueueMutex
								 , std::condition_variable& i_QueueCV
								 , std::queue<QueueItem>& i_Queue
								 , unsigned int& i_NumWriters );
		
	};
}

#endif /* defined(__eloquent__FileWriterFactory__) */