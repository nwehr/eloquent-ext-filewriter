//
// Copyright 2013-2014 EvriChart, Inc. All Rights Reserved.
// See LICENSE.txt
//

// C++
#include <mutex>

// Internal
#include "FileWriter.h"

///////////////////////////////////////////////////////////////////////////////
// FileWriter : IOExtension
///////////////////////////////////////////////////////////////////////////////
Eloquent::FileWriter::FileWriter( const boost::property_tree::ptree::value_type& i_Config
								 , std::mutex& i_QueueMutex
								 , std::condition_variable& i_QueueCV
								 , std::queue<QueueItem>& i_Queue
								 , unsigned int& i_NumWriters )
: IO( i_Config, i_QueueMutex, i_QueueCV, i_Queue, i_NumWriters )
, m_FilePath( m_Config.second.get<std::string>( "path" ) )
, m_FileStream()	
{
//	syslog( LOG_INFO, "setting up a writer for %s #Comment #Filesystem #Writer #FileWriter", m_FilePath.string().c_str() );
}

Eloquent::FileWriter::~FileWriter() {
	if( m_FileStream.is_open() )
		m_FileStream.close();
	
//	syslog( LOG_INFO, "shutting down a writer for %s #Comment #Filesystem #Writer #FileWriter", m_FilePath.string().c_str() );
	
}

void Eloquent::FileWriter::operator()() {
	while( true ) {
		try {
			QueueItem& Item = NextQueueItem();
			
			if( !m_FileStream.is_open() )
				m_FileStream.open( m_FilePath.string().c_str(), std::ofstream::out | std::ofstream::app );
			
			m_FileStream << Item.Data() << std::endl;
			
			m_FileStream.flush();
			
		} catch( const std::exception& e ) {
			syslog( LOG_ERR, "%s #Error #Writer #FileWriter", e.what() );
		} catch( ... ) {
			syslog( LOG_ERR, "unknown exception #Error #Attention #Writer #FileWriter" );
			
			delete this;
			
		}

	}
	
}
