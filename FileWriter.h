#ifndef _FileWriter_h
#define _FileWriter_h

//
// Copyright 2013-2014 EvriChart, Inc. All Rights Reserved.
// See LICENSE.txt
//

// C
#include <syslog.h>

// C++
#include <sstream>
#include <iostream>
#include <fstream>

// Boost
#include <boost/property_tree/ptree.hpp>
#include <boost/smart_ptr.hpp>
#include <boost/filesystem.hpp>

// Internal
#include "Eloquent/Extensions/IO/IO.h"

namespace Eloquent {
	///////////////////////////////////////////////////////////////////////////////
	// FileWriter : IOExtension
	///////////////////////////////////////////////////////////////////////////////
	class FileWriter : public IO {
		FileWriter();
	public:
		explicit FileWriter( const boost::property_tree::ptree::value_type& i_Config
							, std::mutex& i_QueueMutex
							, std::condition_variable& i_QueueCV
							, std::queue<QueueItem>& i_Queue
							, unsigned int& i_NumWriters );

		virtual ~FileWriter();
		virtual void operator()();

	private:
		// File Location/Data
		boost::filesystem::path m_FilePath;
		std::ofstream 			m_FileStream;
	};
}

#endif // _FileWriter_h