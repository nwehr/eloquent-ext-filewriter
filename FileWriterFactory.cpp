#include "FileWriterFactory.h"

///////////////////////////////////////////////////////////////////////////////
// FileWriterFactory : IOExtensionFactory
///////////////////////////////////////////////////////////////////////////////
Eloquent::FileWriterFactory::FileWriterFactory() {}
Eloquent::FileWriterFactory::~FileWriterFactory() {}

Eloquent::IO* Eloquent::FileWriterFactory::New( const boost::property_tree::ptree::value_type& i_Config
														, std::mutex& i_QueueMutex
														, std::condition_variable& i_QueueCV
														, std::queue<QueueItem>& i_Queue
														, unsigned int& i_NumWriters )
{
//	syslog( LOG_DEBUG, "returning new writer #Comment #Factory #FileWriterFactory" );
	return new FileWriter( i_Config, i_QueueMutex, i_QueueCV, i_Queue, i_NumWriters );
}